# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('judul', models.CharField(max_length=100)),
                ('deskripsi', models.CharField(max_length=1200)),
            ],
        ),
        migrations.CreateModel(
            name='Akun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Barang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=100)),
                ('jumlah', models.CharField(null=True, max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('deadline', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_buku',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('keterangan', models.CharField(null=True, max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_donatur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('tanggal', models.DateField()),
                ('konfirmasi', models.BooleanField()),
                ('via', models.CharField(null=True, max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_uang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('bank', models.CharField(null=True, max_length=50)),
                ('jumlah_uang', models.IntegerField()),
                ('bukti_transfer', models.FileField(upload_to='static/foto/bukti_transfer')),
                ('donasi_donatur', models.ForeignKey(to='rango.Donasi_donatur')),
            ],
        ),
        migrations.CreateModel(
            name='Donatur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=50)),
                ('email', models.CharField(null=True, max_length=50)),
                ('nohp', models.CharField(null=True, max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='FileMailTemp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('fileUpload', models.FileField(upload_to='static/files')),
            ],
        ),
        migrations.CreateModel(
            name='FotoProgram',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('gambar', models.FileField(upload_to='static/images/proyek')),
                ('caption', models.CharField(default='', max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Laporan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('keterangan', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Laporan_item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('keterangan', models.CharField(max_length=1000)),
                ('nominal', models.IntegerField()),
                ('isDebit', models.BooleanField()),
                ('isKredit', models.BooleanField()),
                ('laporan', models.ForeignKey(to='rango.Laporan')),
            ],
        ),
        migrations.CreateModel(
            name='Laporan_temp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('filecontent', models.FileField(upload_to='static/files')),
            ],
        ),
        migrations.CreateModel(
            name='Nama',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('namaDepan', models.CharField(validators=[django.core.validators.RegexValidator('^[a-zA-Z]*$', 'Only alphanumeric characters are allowed.')], max_length=100)),
                ('namaBelakang', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=100)),
                ('deskripsi', models.TextField()),
                ('target', models.CharField(max_length=100)),
                ('partner', models.CharField(max_length=100)),
                ('logoPartner', models.FileField(upload_to='static/images/logo')),
            ],
        ),
        migrations.CreateModel(
            name='Rekening',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=50)),
                ('noRekening', models.CharField(max_length=50)),
                ('bank', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('a', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tim',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('nama', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('jabatan', models.CharField(max_length=200)),
                ('foto', models.FileField(upload_to='static/images/tim')),
            ],
        ),
        migrations.AddField(
            model_name='laporan',
            name='program',
            field=models.ForeignKey(to='rango.Program'),
        ),
        migrations.AddField(
            model_name='fotoprogram',
            name='program',
            field=models.ForeignKey(to='rango.Program'),
        ),
        migrations.AddField(
            model_name='donatur',
            name='program',
            field=models.ForeignKey(to='rango.Program'),
        ),
        migrations.AddField(
            model_name='donasi_donatur',
            name='donatur',
            field=models.ForeignKey(to='rango.Donatur'),
        ),
        migrations.AddField(
            model_name='donasi_buku',
            name='donasi_donatur',
            field=models.ForeignKey(to='rango.Donasi_donatur'),
        ),
        migrations.AddField(
            model_name='donasi',
            name='program',
            field=models.ForeignKey(to='rango.Program'),
        ),
        migrations.AddField(
            model_name='donasi',
            name='rekening',
            field=models.ForeignKey(to='rango.Rekening'),
        ),
        migrations.AddField(
            model_name='barang',
            name='program',
            field=models.ForeignKey(to='rango.Program'),
        ),
    ]
