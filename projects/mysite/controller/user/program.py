from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from rango.models import *
from mysite.controller.admin.login import auth

def index(request):

    donasi = Donasi.objects.all()
    foto   = FotoProgram.objects.all()
    donasi_selected = None


    for d in donasi :
        if d.belum_lewat_deadline():
            donasi_selected = d

    if donasi_selected == None:
        proyek_selected = None
        title           = 'Proyek Saat Ini'
    else:
        proyek_selected = donasi_selected.program
        title           = proyek_selected.nama

    data = {'program': Program.objects.all().order_by('-id'),'program_selected':proyek_selected, 'fotoProgram': foto, 'login': auth(request), 'title':title, "aktif_proyek":"active"}
    return render(request, 'proyek.html', data)

def all(request):
    data = {'program': Program.objects.all(), 'fotoProgram': FotoProgram.objects.all(), 'login': auth(request), 'title':"Proyek"}
    return render(request, 'seeall.html', data)


def detil(request):
    if request.method == "GET":
        programId 				= request.GET.get('programId')
        proyek_selected 		= get_object_or_404(Program, pk=programId)
        proyek 					= Program.objects.all().order_by("-id")
        proyek_other 			= []

        fotoProgram_selected 	= FotoProgram.objects.filter(program=programId)
        donasi_selected 		= Donasi.objects.filter(program=programId)

        laporan = Laporan.objects.get(program=proyek_selected)
        counter					= 2
        for p in proyek:
            if not p == proyek_selected and counter > 0:
                proyek_other.append(p)
                counter = counter - 1

        data = {'proyek_selected': proyek_selected, 'proyek_other': proyek_other,
                'fotoProgram_selected': fotoProgram_selected,'donasi_selected': donasi_selected,
                'title': proyek_selected.nama, 'laporan': laporan}

        return render(request, 'detilProyek.html', data)
