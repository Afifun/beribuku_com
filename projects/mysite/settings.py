"""
Django settings for mysite project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
SETTINGS_DIR = os.path.dirname(__file__)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_PATH = os.path.dirname(os.path.dirname(__file__))

PROJECT_PATH = os.path.join(SETTINGS_DIR, os.pardir)
PROJECT_PATH = os.path.abspath(PROJECT_PATH)

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) 


TEMPLATE_PATH = os.path.join(PROJECT_PATH, 'mysite/template')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i5-eqbviq&+5y0tsnr^-^g7%#2@l7w6dtn9kxywfid94k)8+7f'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

TEMPLATE_DIRS = (
    TEMPLATE_PATH,
)

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rango'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'mysite.urls'

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASE_PATH = os.path.join(PROJECT_PATH, 'rango.db')
DATABASES = {
    'default': {
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': DATABASE_PATH,

        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'beribukudb',
        'USER': 'beribukuadmin',
        'PASSWORD': 'admin;buku;1000',
        'HOST': 'localhost',
        'ATOMIC_REQUESTS': True,
    }
}

SESESSION_COOKIE_AGE = 24 * 60 * 60

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/
STATIC_PATH = os.path.join(PROJECT_PATH,'static')

STATIC_URL = '/static/' # You may find this is already defined as such.

# MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'static')
# MEDIA_URL = '/static/'

STATICFILES_DIRS = (
    STATIC_PATH,
)

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.ui.ac.id'
EMAIL_HOST_USER = 'moh.afifun@ui.ac.id'
EMAIL_HOST_PASSWORD = 'mamli195211'
DEFAULT_FROM_EMAIL = 'moh.afifun@ui.ac.id'
SERVER_EMAIL = 'moh.afifun@ui.ac.id'
EMAIL_PORT='25'

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
