from django.conf.urls import include, url
from django.conf.urls import *
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

handler404  = 'mysite.controller.user.index.handler404'

urlpatterns = [

    url(r'^$', 'mysite.controller.user.index.index', name='index'),
    url(r'^home/', 'mysite.controller.user.index.home', name='home'),

    url(r'^program/', 'mysite.controller.user.program.index', name='program'),
    url(r'^detil/$', 'mysite.controller.user.program.detil', name='program.detil'),
    url(r'^allProgram/$', 'mysite.controller.user.program.all', name='program.all'),

    url(r'^about/', 'mysite.controller.user.index.about', name='about'),
    url(r'^tim/', 'mysite.controller.user.index.tim', name='tim'),
    url(r'^contact/', 'mysite.controller.user.index.contact', name='contact'),
    url(r'^addDonatur/$', 'mysite.controller.user.index.addDonatur', name='user.donatur.new'),

    # url admin
    
    url(r'^admin/login/', 'mysite.controller.admin.login.index', name='admin.login'),
    url(r'^admin/logout/', 'mysite.controller.admin.login.logout', name='admin.logout'),

    url(r'^admin/home/', 'mysite.controller.admin.main.home', name='admin.home'),

    url(r'^admin/master/', 'mysite.controller.admin.master.index', name='admin.master'),
    url(r'^admin/newTim/', 'mysite.controller.admin.master.addNewTim', name='admin.master.new'),
    url(r'^admin/editTim/$', 'mysite.controller.admin.master.editTim', name='admin.master.tim.edit'),
    url(r'^admin/timDelete/$', 'mysite.controller.admin.master.deleteTim', name='admin.master.deleteTim'),

    url(r'^admin/newRekening/', 'mysite.controller.admin.master.addNewRekening', name='admin.master.newRekening'),
    url(r'^admin/editRekening/$', 'mysite.controller.admin.master.editRekening', name='admin.master.rekening.edit'),
    url(r'^admin/rekDelete/$', 'mysite.controller.admin.master.deleteRekening', name='admin.master.deleteRekening'),
    
    url(r'^admin/newAbout/', 'mysite.controller.admin.master.addNewAbout', name='admin.master.newAbout'),
    url(r'^admin/aboutDelete/$', 'mysite.controller.admin.master.deleteAbout', name='admin.master.deleteAbout'),

    url(r'^admin/mail/lpj/(?P<programId>\d+)/$', 'mysite.controller.admin.main.mail_lpj', name='admin.mail.lpj'),
    url(r'^admin/mail/proposal/(?P<programId>\d+)/$', 'mysite.controller.admin.main.mail_proposal', name='admin.mail.proposal'),

    url(r'^admin/program/$', 'mysite.controller.admin.program.index', name='admin.program'),
    url(r'^admin/program/(?P<alert>True)/(?P<alert_msg>.*)/$', 'mysite.controller.admin.program.index', name='admin.program'),
    url(r'^admin/new/', 'mysite.controller.admin.program.addNewProgram', name='admin.program.new'),
    url(r'^admin/view/$', 'mysite.controller.admin.program.viewProgram', name='admin.program.view'),
    url(r'^admin/delete/$', 'mysite.controller.admin.program.deleteProgram', name='admin.program.delete'),
    url(r'^admin/edit/$', 'mysite.controller.admin.program.editProgram', name='admin.program.edit'),
    url(r'^admin/exceldonatur/(?P<programId>\d+)/$', 'mysite.controller.admin.program.downloadExcelDonatur', name='admin.program.exceldonatur'),
    url(r'^admin/word/(?P<programId>\d+)/$', 'mysite.controller.admin.program.downloadWord', name='admin.program.word'),

    url(r'^admin/donatur/$', 'mysite.controller.admin.donatur.index', name='admin.donatur'),
    url(r'^admin/donatur/(?P<alert>True)/(?P<alert_msg>.*)/$', 'mysite.controller.admin.donatur.index', name='admin.donatur'),
    url(r'^admin/addDonatur/$', 'mysite.controller.admin.donatur.addDonatur', name='admin.donatur.new'),
    url(r'^admin/addDonatur/(?P<programId>\d+)/$', 'mysite.controller.admin.donatur.addSpecDonatur', name='admin.donatur.newspec'),
    url(r'^admin/deleteDonatur/$', 'mysite.controller.admin.donatur.deleteDonatur', name='admin.donatur.delete'),
    url(r'^admin/editDonatur/$', 'mysite.controller.admin.donatur.editDonatur', name='admin.donatur.edit'),

    url(r'^admin/donasi/new/(?P<programId>\d+)/$', 'mysite.controller.admin.donasi.addDonasi', name='admin.donasi.new'),

    url(r'^admin/donasi/uang/view/(?P<programId>\d+)/$', 'mysite.controller.admin.donasi.viewDonasiUang', name='admin.donasi.uang.view'),
    url(r'^admin/donasi/uang/view/(?P<programId>\d+)/(?P<alert>True)/(?P<alert_msg>.*)/$', 'mysite.controller.admin.donasi.viewDonasiUang', name='admin.donasi.uang.view'),
    url(r'^admin/donasi/uang/delete/(?P<donasi_id>\d+)/$', 'mysite.controller.admin.donasi.deleteDonasiUang', name='admin.donasi.uang.delete'),
    url(r'^admin/donasi/uang/edit/(?P<donasi_id>\d+)/$', 'mysite.controller.admin.donasi.editDonasiUang', name='admin.donasi.uang.edit'),

    url(r'^admin/donasi/buku/view/(?P<programId>\d+)/$', 'mysite.controller.admin.donasi.viewDonasiBuku', name='admin.donasi.buku.view'),
    url(r'^admin/donasi/buku/view/(?P<programId>\d+)/(?P<alert>True)/(?P<alert_msg>.*)/$', 'mysite.controller.admin.donasi.viewDonasiBuku', name='admin.donasi.buku.view'),
    url(r'^admin/donasi/buku/delete/(?P<donasi_id>\d+)/$', 'mysite.controller.admin.donasi.deleteDonasiBuku', name='admin.donasi.buku.delete'),
    url(r'^admin/donasi/buku/edit/(?P<donasi_id>\d+)/$', 'mysite.controller.admin.donasi.editDonasiBuku', name='admin.donasi.buku.edit'),
    
    url(r'^admin/donasi/excel/new/(?P<programId>\d+)/$', 'mysite.controller.admin.donasi.addDonasiExcel', name='admin.donasi.new.excel'),

    url(r'^admin/laporan/$', 'mysite.controller.admin.laporan.index', name='admin.laporan.index'),
    url(r'^admin/laporan/realisasi/(?P<programId>\d+)/$', 'mysite.controller.admin.laporan.addKeteranganLaporan', name='admin.laporan.add.keterangan'),
    url(r'^admin/addLaporan/$', 'mysite.controller.admin.laporan.addLaporan', name='admin.laporan.new'),

        
] + static(settings.STATIC_URL, document_root=settings.STATIC_PATH)

if settings.DEBUG is False:   #if DEBUG is True it will be served automatically
    urlpatterns += patterns('',
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_PATH}),
    )