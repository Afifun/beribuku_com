$(document).ready(function(){

	$('.btn-gambar').click(
		function(e){
			$('.div-gambar').append(
			'<div class="row"><div class="col-sm-3"><input type="file" class="form-control" name="gambar" id="" placeholder=""><p class="help-block">Unggah file gambar dengan jenis jpg, png, atau gif</p></div><div class="col-sm-9"><input type="text" class="form-control" name="gambarcaption" id="" placeholder="Keterangan gambar (opsional)"><a href="#" class="remove">Remove</a></div></div>');
	});

	$('.div-gambar').on("click", ".remove", function(e){
		$(this).parent('div').parent('div').remove();
	});

	$('.btn-item').click(
		function(e){
			$('.div-item').append('<div><div class="col-sm-6"><input type="text" name="barang"  class="form-control" id="" placeholder="Item"></div><div class="col-sm-6"><input type="text" name="jumlahBarang" class="form-control" id="" placeholder="Jumlah (optional)"><a href="#" class="remove">Remove</a></div></div>');
	});

	$('.div-item').on("click", ".remove", function(e){
		$(this).parent('div').parent('div').remove();
	});

});