# README #

# Developement Guide #
## Pre-requisites ##
* [**Python 3.4.3**](https://www.python.org/downloads/) atau yang lebih baru. 
* **Git**. Seperti github atau yang lainnya 
* **Django 1.8** atau yang lebih baru
* **postgresql-server** **libpg-dev** **pgadmin3** di ubuntu 
* module **psycopg2** untuk python3 

Untuk melakukan development, ikuti langkah-langkah berikut:

## Clone Project ##
Clone project Beribuku_com di 


```
#!URL
https://Afifun@bitbucket.org/Afifun/beribuku_com.git 

```

Menjalankan Project:

1. Buka direktori beribuku_com dengan cmd atau terminal.
2. Jalankan perintah 

```
#!python

python manage.py runserver
```

**TODO:** Mengikuti dua langkah tersebut diatas masih belum cukup untuk dapat langsung menjalankan server. Masih perlu beberapa langkah-langkah terkait database dan autentikasi awal.

**Konfirmasi Hasil:** Bila langkah-langkah sudah lengkap dan benar, secara default aplikasi **beribuku** akan berjalan pada URL:

```
#!URL
<nama domain>:8000

```


## Panduan Struktur Berkas dan Direktori ##

### Struktur Direktori ###
Terdapat tujuh buah direktori yang berfungsi menyimpan data-data yang berbeda. Ketujuh direktori ini adalah:

1. projects
2. xlrd-0.9.3
3. xlwt-future-0.8.0

Dari ketiga direktori tersebut yang akan dijelaskan pada bagian selanjutnya hanya direktori projects.

### Direktori projects ###
Direktori projects merupakan root direktori, yaitu terdiri dari 3 direktori, yaitu:

1. Mysite - aplikasi
2. Rango - model
3. Static – gambar, css, berkas resource
 
Apa yang terdapat dalam masing-masing direktori akan dijelaskan pada bagian selanjutnya

### Direktori mysite ###

Direktori mysite adalah direktori aplikasi, yaitu yang terdiri dari 2 direktori :

**1. Controller** 
Direktori ini berisi tentang controller. Direktori ini terdiri dari 2 direktori, yaitu  Admin dan User. Direktori Admin berisi tentang controller untuk halaman admin, dan direktori user berisi tentang controller untuk halaman user.
 
**2. Template** 
Direktori ini berisi tentang view. Direktori ini terdiri dari 1 direktori admin yang mengatur tentang view pada halaman khusus admin.
 
### Direktori rango ###
Direktori rango terdiri dari 2 direktori, yaitu:

1. Migrations
2. Templatetags 


### Direktori static ###

Direktori static terdiri dari 6 direktori, yaitu :

1. css
Direktori css berisi file css untuk tema halaman web, data tabel, huruf, jquery dan tampilan halaman web.

2. files
Direktori files berisi 4 file terkait isi web, yaitu manual_admin.pdf, template.xlsx (template excel), template_upload_donatur_kode.xlsx.

3. fonts
Direktori fonts berisi tentang jenis huruf yang digunakan dalam tampilan web. Jenis huruf yang digunakan dalam tampilan halaman web adalah aparajbi.ttf dan glyphicons-haflings-regular.ttf.

4. foto/bukti_transfer
Direktori foto/bukti_transfer disediakan untuk menyimpan foto/bukti transfer donasi dari donatur yang diupload melalui halaman web.


5. js
Direktori js berisi file javascript untuk mengembangkan web. 
Direktori ini terdiri dari direktori tinymce yang tersusun dalam 4 direktori, yaitu: *langs, plugins, skins/lightgray, themes/moderns*

6. images
Direktori images berisikan gambar-gambar yang dibutuhkan untuk ditampilkan pada halaman web, seperti logo beri nuku, gambar slide, foto tim beri buku, dst. Direktori ini terdiri dari 4 direktori, yaitu:

   * bukti: Direktori bukti berisi gambar/foto bukti transfer donatur.
   * logo: Direktori logo berisi logo yang akan ditampilkan dalam halaman web.
   * proyek: Direktori proyek berisi foto-foto proyek / kegiatan yang dilakukan oleh beri buku.
   * tim: Direktori tim berisi foto-foto  dari anggota tim beri buku.

Bila catatan ini keliru atau ada kekurangan, silahkan sampaikan ke fmse@cs.ui.ac.id.

Semoga bermanfaat.